DROP TABLE IF EXISTS cars;

CREATE TABLE cars (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  manufacturer VARCHAR(250) NOT NULL,
  model VARCHAR(250) NOT NULL,
  build YEAR DEFAULT NULL
);

INSERT INTO cars (manufacturer, model, build) VALUES
  ('Graystone Industries', 'Viper ProtoType I', 2388),
  ('Graystone Industries', 'Viper MKI', 2399),
  ('Graystone Industries', 'Viper MK II', 2417),
  ('Graystone Industries', 'Viper MK III', 2419),
  ('Vergis Corporation', 'Viper MK VI', 2421),
  ('Vergis Corporation', 'Viper MK V', 2421),
  ('Caprica LMTD', 'Viper MK VI', 2422),
  ('Caprica LMTD', 'Viper MK VII', 2424),
  ('Caprica LMTD', 'Viper MK VIII', 2432);
